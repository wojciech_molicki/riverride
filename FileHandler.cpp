#include "FileHandler.h"

CFileHandler::CFileHandler(void)
{
}

CFileHandler::~CFileHandler(void)
{
}

int CFileHandler::loadMapFromFile(std::string path, std::vector<int> &returnVec)
{
	std::ifstream fileHandle;
	std::string line;
	std::string number;

	fileHandle.open(path.c_str());

	if (!fileHandle.good())
	{
		std::cerr << "file load error" ;
		return 1;
	}
	else
	{
		while(fileHandle >> line)
		{
			std::stringstream iss(line);
			while (iss >> number)
				returnVec.push_back(std::stoi(number));
		}
	}
	std::reverse(returnVec.begin(), returnVec.end());
	fileHandle.close();
	return 0;
}