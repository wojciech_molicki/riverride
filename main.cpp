#include "Game.h"

int main(int argc, char** argv) 
{
	CGame game;
	game.init();

	while(game.running())
	{
		game.update();
		game.render();
	}

	game.cleanup();

	return 0;
}