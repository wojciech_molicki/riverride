#ifndef GLOBALS_H
#define GLOBALS_H

const int TILE_WIDTH = 25;
const int TILE_HEIGHT = 25;

const int TILE_ROWS = 512;
const int TILE_COLUMNS = 32;

const int TILE_SPRITES = 6;
const int TILE_GREEN = 1;
const int TILE_WATER = 0;

enum destroyables { STATIC_BOAT = 1, MOVING_BOAT, HELICOPTER, STATIC_TANK_L, STATIC_TANK_R,
		FIGHTER, MOVING_TANK, BRIDGE, POWERUP_FUEL };

const int LEVEL_WIDTH = 800;
const int LEVEL_HEIGHT = TILE_ROWS * TILE_HEIGHT;

const int TILE_NUMBER = TILE_ROWS * TILE_COLUMNS;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int PLANE_WIDTH = 32;
const int PLANE_HEIGHT = 37;

const int PLANE_MAX_SPEED = -7;
const int PLANE_REGULAR_SPEED = -4;
const int PLANE_MIN_SPEED = -2;

const int PLANE_SHELL_SPEED = 11;
const int PLANE_SHELL_FREQ = 100;

#endif