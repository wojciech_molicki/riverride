#ifndef ENEMY_H
#define ENEMY_H

#include "SDL.h"
#include "Globals.h"
#include <list>
#include "Timer.h"
#include "Shell.h"
#include "Window.h"
#include "Plane.h"
#include <memory>

class CEnemy {
protected:
	SDL_Rect clip;

	Timer timer;

	bool alive;
	bool shooting;

public:
	CEnemy();
	~CEnemy();

	SDL_Rect box;

	void setBox(int x, int y, int w, int h);
	void setClip(int x, int y, int w, int h);

	void move();
	void switchDirection();
	void switchShootingOn();
	virtual void show(SDL_Texture *tex, CPlane &plane) = 0;
	virtual void shoot(std::list<std::unique_ptr<CShell>> &enemyShells) = 0;

	void ressurect();
	void kill();

	bool isMoving();
	bool isShooting();
	bool isAlive();

	int Vx;
	int type;
	int direction;

	SDL_Rect getBox();
	SDL_Rect getClip();

	void switchMovementOn(int direction);

private:

	//std::list<
};

class CBoat : public CEnemy {
public:
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
	void show(SDL_Texture *tex, CPlane &plane);
	CBoat();

private:
};

class CFuel : public CEnemy {
public:
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
	void show(SDL_Texture *tex, CPlane &plane);
	CFuel();

private:
};

class CBridge : public CEnemy {
public:
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
	void show(SDL_Texture *tex, CPlane &plane);
	CBridge();
	bool closest;
private:
};

class CHeli : public CEnemy {
public:
	void show(SDL_Texture *tex, CPlane &plane);
	CHeli();
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
private:
};

class CTank : public CEnemy {
public:

	CTank();
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
	void show(SDL_Texture *tex, CPlane &plane);
private:
};

class CFighter : public CEnemy {
public:

	CFighter();
	void shoot(std::list<std::unique_ptr<CShell>> &enemyShells);
	void show(SDL_Texture *tex, CPlane &plane);
private:
};

#endif