#include "Game.h"

CGame::CGame(void)
{
}

CGame::~CGame(void)
{
}

void CGame::init() {
	Window::Init();

	clip_image = Window::LoadImage("./res/res.png");
	fontcolor.r = 255; fontcolor.g = 255; fontcolor.b = 0; fontcolor.a = 0;
	score_tex = Window::RenderText("00000000", Window::font, fontcolor, fontcolor, 2);

	m_bRunning = true;
	score = 0;
	frameLimiter = 0;
	gamestate = START;

	menu_fighter.setBox(0, 120, menu_fighter.getClip().w, menu_fighter.getClip().h);
	menu_fighter.switchMovementOn(3); menu_heli.switchMovementOn(1);
	menu_heli.setBox(SCREEN_WIDTH - 50, 220, menu_heli.getClip().w, menu_heli.getClip().h);

	timer.Start();
}

void CGame::render()
{
	frameLimiter++;
	Window::Clear();
	switch (gamestate)
	{
	case GAME:
		drawTiles();

		handleShots();
		drawPowerups();
		drawEnemies();
		drawEnemyShells();

		drawBottomBar();
		plane.show(clip_image);
		break;
	case START:
		drawMenus();
		break;
	case GAMEOVER:
		drawGameOverScreen();
		break;
	}
	Window::Present();
	if (timer.Ticks() > 1000)
	{
		Window::ChangeWindowTitle("FPS: " + std::to_string(frameLimiter));
		timer.Restart();
		frameLimiter = 0;
	}
	if (frameLimiter > 60) { SDL_Delay(20); }
}
void CGame::update()
{
	switch (gamestate)
	{
	case GAME:
		handleEvents();
		plane.move();
		plane.setCamera();

		enemyMovement();
		collideShellsWithEnemies();
		collideShellsWithPowerups();
		powerupsCollision();
		handleEnemyShots();

		if (groundCollision(plane.getBox()) || enemyCollision(plane.getBox()) || collidePlaneWithEnemyShells() || (plane.getFuel() == 0))
		{
			plane.removeLife(); resetLevel();
		}
		if (plane.getLives() == 0)
		{
			gameOverHandler();
			gamestate = GAMEOVER;
		}
		break;
	case START:
		handleMenuEvents();
		break;
	case GAMEOVER:
		handleMenuEvents();
		break;
	}
}

void CGame::handleEvents()
{
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE: m_bRunning = false; break;
			case SDLK_UP: plane.setVy(PLANE_MAX_SPEED);	break;
			case SDLK_DOWN: plane.setVy(PLANE_MIN_SPEED); break;
			case SDLK_LEFT: plane.setVx(-1);  break;
			case SDLK_RIGHT: plane.setVx(1); break;
			case SDLK_SPACE: plane.setFire(1); break;
			};
			break;
		case SDL_KEYUP:
			switch (e.key.keysym.sym)
			{
			case SDLK_SPACE: plane.setFire(0); break;
			case SDLK_UP: plane.resetVyUp(); break;
			case SDLK_DOWN: plane.resetVyDown(); break;
			case SDLK_LEFT: plane.resetVxLeft(); break;
			case SDLK_RIGHT: plane.resetVxRight(); break;
			};
			break;
		case SDL_QUIT:m_bRunning = false;
			break;
		};
	}
}

void CGame::handleMenuEvents()
{
	//handles events while not in game
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE: m_bRunning = false; break;
			case SDLK_SPACE:
				gamestate = GAME;
				{
					timer.Start();
					fontcolor.r = 255; fontcolor.g = 255; fontcolor.b = 0; fontcolor.a = 0;
					score = 0;
					updateScoreTexture(0);
					m_bRunning = true;

					frameLimiter = 0;
					loadInitial();
					setTilePlaces();
					setTileTypes();
					plane.reset();
				}
				break;
			};
			break;
		case SDL_KEYUP:
			switch (e.key.keysym.sym)
			{
			case SDLK_SPACE: break;
			};
			break;
		case SDL_QUIT: m_bRunning = false;
			break;
		};
	}
}

void CGame::loadInitial() {
	CFileHandler fh;
	fh.loadMapFromFile("./res/map4.txt", mapVector);
}

void CGame::setTileTypes() {
	for (int i = 0; i < TILE_NUMBER; i++)
	{
		//ground types ranging from 0 to 9, enemy types can begin on all ground types
		board[i].set_type(mapVector.back() % 10);
		if (mapVector.back() > 9)
		{
			switch (mapVector.back() / 10) {
			case STATIC_BOAT:
				{
					std::unique_ptr<CBoat> enemy_ptr(new CBoat());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case MOVING_BOAT:
				{
					std::unique_ptr<CBoat> enemy_ptr(new CBoat());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchMovementOn(1);
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case HELICOPTER:
				{
					std::unique_ptr<CHeli> enemy_ptr(new CHeli());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchMovementOn(1);
					enemy_ptr->switchShootingOn();
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case STATIC_TANK_L:
				{
					std::unique_ptr<CTank> enemy_ptr(new CTank());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchShootingOn();
					enemy_ptr->direction = -1;
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case STATIC_TANK_R:
				{
					std::unique_ptr<CTank> enemy_ptr(new CTank());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchShootingOn();
					enemy_ptr->direction = 1;
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case POWERUP_FUEL:
				{
					std::unique_ptr<CFuel> powerup_ptr(new CFuel());
					powerup_ptr->setBox(board[i].get_box().x, board[i].get_box().y, powerup_ptr->getClip().w, powerup_ptr->getClip().h);
					powerupList.push_back(std::move(powerup_ptr));
				} break;
			case BRIDGE:
				{
					std::unique_ptr<CBridge> enemy_ptr(new CBridge());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case FIGHTER:
				{
					std::unique_ptr<CFighter> enemy_ptr(new CFighter());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchShootingOn();
					enemy_ptr->switchMovementOn(2);
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			case MOVING_TANK:
				{
					std::unique_ptr<CTank> enemy_ptr(new CTank());
					enemy_ptr->setBox(board[i].get_box().x, board[i].get_box().y, enemy_ptr->getClip().w, enemy_ptr->getClip().h);
					enemy_ptr->switchMovementOn(1);
					enemy_ptr->switchShootingOn();
					enemy_ptr->type = MOVING_TANK;
					enemyList.emplace_back(std::move(enemy_ptr));
				} break;
			}
		}
		mapVector.pop_back();
	}
}

void CGame::drawEnemies() {
	for (std::list<std::unique_ptr<CEnemy>>::iterator it=enemyList.begin(); it != enemyList.end(); it++)
	{
		if ((*it)->type != MOVING_TANK)
			(*it)->show(clip_image, plane);
	}
	for (std::list<std::unique_ptr<CEnemy>>::iterator it=enemyList.begin(); it != enemyList.end(); it++)
	{
		if ((*it)->type == MOVING_TANK)
			(*it)->show(clip_image, plane);
	}
}

void CGame::setTilePlaces() {
	for (int i = 0; i < TILE_ROWS; i++)
	{
		for (int j = 0; j < TILE_COLUMNS; j++)
		{
			board[i*TILE_COLUMNS+j].set_pos(TILE_WIDTH*j, TILE_HEIGHT*i);
		}
	}
}
void CGame::drawTiles()
{
	for (int i = 0; i < TILE_NUMBER; i++)
	{
		SDL_Rect camera = plane.getCamera();
		SDL_Rect dest = board[i].get_box();
		//checks if tile is in camera range
		if (dest.y  + 25 > camera.y && dest.y < camera.y + SCREEN_HEIGHT - 150)	{
			dest.x -= camera.x; dest.y -= camera.y;
			Window::Draw(clip_image, dest, &board[i].get_clip());
		}
	}
}

bool CGame::groundCollision(SDL_Rect A) 
{
	//start iterating within camera range
	int start = plane.getCamera().y/TILE_HEIGHT * 32;
	int stop = plane.getCamera().y/TILE_HEIGHT * 32 + 18*32;
	for (int i = start; i < stop; i++)
	{
		if (board[i].get_type() == TILE_GREEN)
		{
			if (checkCollision(A, board[i].get_box()) == true)
				return true;
		}
	}
	return false;
}

bool CGame::enemyCollision(SDL_Rect A) {
	for (std::list<std::unique_ptr<CEnemy>>::iterator enemy = enemyList.begin(); enemy != enemyList.end(); enemy++)
	{
		if ((*enemy)->isAlive())
		{
			if (checkCollision(A, (*enemy)->getBox()) == true)
			{
				return true;
			}
			//enemies shooting
			if ((*enemy)->isShooting() && checkCollision(plane.getCamera(), (*enemy)->getBox()))
				(*enemy)->shoot(enemyShells);
		}
	}
	return false;
}

void CGame::enemyMovement()
{
	for (std::list<std::unique_ptr<CEnemy>>::iterator it = enemyList.begin(); it != enemyList.end(); it++)
	{
		if ( (*it)->isAlive() && (*it)->isMoving() && checkCollision(plane.getCamera(), (*it)->getBox()))
		{
			(*it)->move();
			if ((*it)->type != FIGHTER && (*it)->type != MOVING_TANK && groundCollision((*it)->getBox()))
			{
				(*it)->switchDirection();
			}
			if ((*it)->type == FIGHTER)
			{
				if ((*it)->getBox().x >= LEVEL_WIDTH)
					(*it)->box.x = 0;
			}
			if ((*it)->type == MOVING_TANK)
			{
				if ((*it)->getBox().x > 650)
				{
					(*it)->switchDirection();
				}
				if ((*it)->getBox().x < 150)
				{
					(*it)->switchDirection();
				}
				//check collision with collapsing bridge
				if (standingOnCollapsingBridge((*it)->getBox()))
				{
					//kill tank if standing on brigde
					(*it)->kill();
					updateScoreTexture(2000);
				}
				//immobilise tank if bridge is dead
				if (bridgeDead())
				{
					(*it)->switchMovementOn(0);
				}
			}
		}
	}
}

bool CGame::standingOnCollapsingBridge(SDL_Rect A)
{
	for (std::list<std::unique_ptr<CEnemy>>::iterator it = enemyList.begin(); it != enemyList.end(); it++)
	{
		//checks all bridges on screen, and returns whether A is standing on dead brigde
		if ((*it)->type == BRIDGE && checkCollision((*it)->getBox(), plane.getCamera()) && !(*it)->isAlive())
		{
			return checkCollision(A, (*it)->getBox());
		}
	}
	return false;
}

bool CGame::bridgeDead()
{
	for (std::list<std::unique_ptr<CEnemy>>::iterator it = enemyList.begin(); it != enemyList.end(); it++)
	{
		if ((*it)->type == BRIDGE && checkCollision((*it)->getBox(), plane.getCamera()))
			return !(*it)->isAlive();
	}
	return false;
}

void CGame::handleShots()
{
	//get list of plane shells
	std::list<std::unique_ptr<SDL_Rect>> *shells = plane.getShellsFired();
	plane.moveShots();
	for (std::list<std::unique_ptr<SDL_Rect>>::iterator it = shells->begin(); it != shells->end(); it++)
	{
		//render shells as colored rects
		SDL_Rect camera = plane.getCamera();
		SDL_Rect target = {(*it)->x - camera.x, (*it)->y - camera.y, (*it)->w, (*it)->h };
		SDL_SetRenderDrawColor(Window::mRenderer, 65, 255, 255, 0);
		SDL_RenderFillRect(Window::mRenderer, &target);
	}
}

void CGame::handleEnemyShots()
{
	for (std::list<std::unique_ptr<CShell>>::iterator shell = enemyShells.begin(); shell != enemyShells.end(); shell++)
	{
		if ((*shell)->direction > 0)
		{
			//fighter shells are faster
			if ((*shell)->fast == true)
				(*shell)->box.x += 10;
			else
				(*shell)->box.x += 5;
		}
		if ((*shell)->direction < 0)
			(*shell)->box.x -= 5;

		//decrease distance needed for exploding shell to blow
		if ((*shell)->exploding)
		{
			(*shell)->distance -= 5;
			//stop moving
			if ((*shell)->distance < 0)
				(*shell)->direction = 0;
		}

		//if not exploded already and at distance needed for explosion
		if ((*shell)->doIExplode() && !(*shell)->exploded)
		{
			(*shell)->exploded = true;

			//explosion timer
			(*shell)->timer.Start();

			int plane_radius = SDL_sqrt((PLANE_HEIGHT*PLANE_HEIGHT + PLANE_WIDTH*PLANE_WIDTH))/2;
			if (pointToPointDistance(plane.getBox().x + PLANE_WIDTH/2, plane.getBox().y +PLANE_HEIGHT/2, (*shell)->box.x + 2, (*shell)->box.y + 2) < ((*shell)->radius + plane_radius))
			{
				//if distance less than sum of radi, then collision with plane
				plane.removeLife();
				shell->reset();
				enemyShells.erase(shell++);
				continue;
			}
		}

		//if shell goes out of screen
		if (((*shell)->box.x > SCREEN_WIDTH) || ((*shell)->box.x < 0))
		{
			shell->reset();
			enemyShells.erase(shell++);
		}
	}
}

void CGame::drawEnemyShells()
{
	for (std::list<std::unique_ptr<CShell>>::iterator shell = enemyShells.begin(); shell != enemyShells.end(); shell++)
	{
		SDL_Rect camera = plane.getCamera();
		SDL_Rect target = {(*shell)->box.x - camera.x, (*shell)->box.y - camera.y, (*shell)->box.w, (*shell)->box.h };
		if ((*shell)->exploding && ((*shell)->distance < 0) && (*shell)->timer.Ticks() < 500)
		{
			SDL_Rect cl = {150,0,50,50};
			Window::Draw(clip_image, (*shell)->box.x - camera.x - 25, (*shell)->box.y - camera.y - cl.w /2, &cl);
		}
		else if ((*shell)->exploding)
			SDL_SetRenderDrawColor(Window::mRenderer, 255, 0, 0, 0);
		else
			SDL_SetRenderDrawColor(Window::mRenderer, 255, 238, 36, 0);
		if ((*shell)->distance > 0)
			SDL_RenderFillRect(Window::mRenderer, &target);
	}
}

void CGame::collideShellsWithEnemies()
{
	//get list of all plane shells
	std::list<std::unique_ptr<SDL_Rect>> *shells = plane.getShellsFired();

	//iterate over enemies
	for (std::list<std::unique_ptr<CEnemy>>::iterator enemy = enemyList.begin(); enemy != enemyList.end(); enemy++)
	{
		//check if enemy is on screen, and is alive
		if (checkCollision(plane.getCamera(), (*enemy)->getBox()) && (*enemy)->isAlive())
		{
			//iterate over plane shells
			for (std::list<std::unique_ptr<SDL_Rect>>::iterator shell = shells->begin(); shell != shells->end(); shell++)
			{
				SDL_Rect shell_rect = {(*shell)->x, (*shell)->y, (*shell)->w, (*shell)->h };
				if (checkCollision(shell_rect, (*enemy)->getBox()))
				{
					shell->release();
					shells->erase(shell++);
					if ((*enemy)->type == BRIDGE)
					{
						//if killed a bridge, set new starting pos
						SDL_Rect bridge = (*enemy)->getBox();
						plane.setStartingPos(bridge.x + bridge.w / 2, bridge.y - bridge.h);
						updateScoreTexture(1000);
					}
					(*enemy)->kill();
					updateScoreTexture(1000);
					break;
				}
			}
		}
	}
}

bool CGame::collidePlaneWithEnemyShells()
{
	for (std::list<std::unique_ptr<CShell>>::iterator it = enemyShells.begin(); it != enemyShells.end(); it++)
	{
		//only non-exploding shells collide with plane
		if (!(*it)->exploding)
		{
			SDL_Rect shell;
			shell.x = (*it)->box.x; shell.y = (*it)->box.y; shell.w = (*it)->box.w; shell.h = (*it)->box.h;
			if (checkCollision(shell, plane.getBox()))
				return true;
		}
	}
	return false;
}

void CGame::collideShellsWithPowerups()
{
	std::list<std::unique_ptr<SDL_Rect>> *shells = plane.getShellsFired();
	for (std::list<std::unique_ptr<CEnemy>>::iterator powerup = powerupList.begin(); powerup != powerupList.end(); powerup++)
	{
		SDL_Rect pwrup = (*powerup)->getBox();
		if (checkCollision(plane.getCamera(), pwrup) && (*powerup)->isAlive())
		{
			for (std::list<std::unique_ptr<SDL_Rect>>::iterator shell = shells->begin(); shell != shells->end(); shell++)
			{
				SDL_Rect target = {(*shell)->x, (*shell)->y, (*shell)->w, (*shell)->h };
				if (checkCollision(target, pwrup))
				{
					shell->reset();
					shells->erase(shell++);
					(*powerup)->kill();
					break;
				}
			}
		}
	}
}

void CGame::powerupsCollision()
{
	for (std::list<std::unique_ptr<CEnemy>>::iterator powerup = powerupList.begin(); powerup != powerupList.end(); powerup++)
	{
		if ((*powerup)->isAlive())
		{
			SDL_Rect target = (*powerup)->getBox();
			if (checkCollision(plane.getBox(), target))
			{
				plane.refuel();
				(*powerup)->kill();
			}
		}
	}
}

void CGame::drawLives()
{
	for (int i = 0; i < plane.getLives(); i++)
	{
		SDL_Rect lifesPlacement = { 50+i*35, 500, PLANE_WIDTH*0.6, PLANE_HEIGHT*0.6 };
		Window::Draw(clip_image, lifesPlacement, &plane.getLivesClip());
	}
}

void CGame::drawPowerups()
{
	SDL_Rect camera = plane.getCamera();

	for (std::list<std::unique_ptr<CEnemy>>::iterator powerup = powerupList.begin(); powerup != powerupList.end(); powerup++)
	{
		if ((*powerup)->isAlive())
		{
			SDL_Rect box = (*powerup)->getBox();
			Window::Draw(clip_image, box.x - camera.x, box.y - camera.y, &(*powerup)->getClip());
		}
	}
}

void CGame::resetLevel()
{
	//resets enemies, powerups
	for (std::list<std::unique_ptr<CEnemy>>::iterator powerup = powerupList.begin(); powerup != powerupList.end(); powerup++)
	{
		(*powerup)->ressurect();
	}
	for (std::list<std::unique_ptr<CEnemy>>::iterator enemy = enemyList.begin(); enemy != enemyList.end(); enemy++)
	{
		if (((*enemy)->type != BRIDGE) && ((*enemy)->type != MOVING_TANK))
			(*enemy)->ressurect();
	}
	for (std::list<std::unique_ptr<CShell>>::iterator shell = enemyShells.begin(); shell != enemyShells.end(); shell++)
	{
		shell->reset();
	}
	enemyShells.clear();
}

void CGame::cleanup()
{
	gameOverHandler();
	plane.reset();
	SDL_DestroyTexture(clip_image);
	SDL_DestroyTexture(score_tex);
	SDL_DestroyTexture(gameover_tex1);
	SDL_DestroyTexture(gameover_tex2);
	Window::Quit();
}

void CGame::drawScore()
{
	Window::Draw(score_tex, SCREEN_WIDTH /2 - 65, 470);
}

void CGame::updateScoreTexture(int value)
{
	score += value;
	std::string x;

	for (int i = 0; i < 8 - std::to_string(score).length(); i++)
	{
		x += '0';
	}
	x += std::to_string(score);

	score_tex = Window::RenderText(x, Window::font, fontcolor, fontcolor, 2);
}

void CGame::drawFuelGauge()
{
	SDL_Rect redBar = {SCREEN_WIDTH/2  - 75 + (int)(plane.getFuel()*1.28), 500, 20, 49 };
	SDL_SetRenderDrawColor(Window::mRenderer, 255, 0, 0, 0);
	SDL_RenderFillRect(Window::mRenderer, &redBar);

	SDL_Rect clip = { 200, 0, 150, 49 };
	Window::Draw(clip_image, SCREEN_WIDTH/2 - 75, 500, &clip);
}

void CGame::drawBottomBar()
{
	//gray bar
	SDL_Rect menu = {0, 450, 800, 150 };
	SDL_SetRenderDrawColor(Window::mRenderer, 171, 171, 171, 0);
	SDL_RenderFillRect(Window::mRenderer, &menu);
	drawLives();
	drawScore();
	drawFuelGauge();
}

void CGame::drawMenus()
{
	menu_fighter.move();
	menu_heli.move();

	if (menu_fighter.getBox().x > SCREEN_WIDTH + 1000)
		menu_fighter.setBox(0, 120, menu_fighter.getClip().w, menu_fighter.getClip().h);
	if (menu_heli.getBox().x + menu_heli.getBox().w > SCREEN_WIDTH || menu_heli.getBox().x < 0)
		menu_heli.switchDirection();

	SDL_Rect clip_menu = {350, 0, 100, 53};
	SDL_Rect target_menu = {250, 100, 300, 159};

	SDL_Rect clip_press = {350, 100, 153, 20};
	SDL_Rect target_press = {250, 400, 306, 40};

	Window::Draw(clip_image, menu_fighter.getBox(), &menu_fighter.getClip(), SDL_FLIP_HORIZONTAL);
	if (timer.Ticks() < 300)
		Window::Draw(clip_image, target_press, &clip_press);
	if (timer.Ticks() > 500)
		timer.Restart();
	Window::Draw(clip_image, target_menu, &clip_menu);

	if (menu_heli.Vx > 0)
		Window::Draw(clip_image, menu_heli.getBox(), &menu_heli.getClip(), SDL_FLIP_HORIZONTAL);
	else
		Window::Draw(clip_image, menu_heli.getBox(), &menu_heli.getClip());
}

void CGame::gameOverHandler()
{
	SDL_Color text_color = {255,255,255,0};
	SDL_SetRenderDrawColor(Window::mRenderer, 0, 0, 0, 0);

	gameover_tex1  = Window::RenderText("Points earned:", Window::font, text_color, text_color, 2);
	gameover_tex2  = Window::RenderText("Press SPACE for new game, to quit hit ESC", Window::font, text_color, text_color, 2);

	for (std::list<std::unique_ptr<CEnemy>>::iterator powerup = powerupList.begin(); powerup != powerupList.end(); powerup++)
	{
		powerup->release();
	}
	for (std::list<std::unique_ptr<CEnemy>>::iterator enemy = enemyList.begin(); enemy != enemyList.end(); enemy++)
	{
		enemy->release();
	}
	for (std::list<std::unique_ptr<CShell>>::iterator shell = enemyShells.begin(); shell != enemyShells.end(); shell++)
	{
		shell->release();
	}
	enemyList.clear();
	enemyShells.clear();
	powerupList.clear();
}

void CGame::drawGameOverScreen()
{
	SDL_Rect clip_gameover = {350, 150, 162, 18};
	SDL_Rect target_gameover = {230, 100, 2*162, 2*18};
	Window::Draw(clip_image, target_gameover, &clip_gameover);

	Window::Draw(gameover_tex1, 240, 250);
	Window::Draw(score_tex, 420, 250);
	Window::Draw(gameover_tex2, 160, 450);
}