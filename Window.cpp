#include "Window.h"

SDL_Window* Window::mWindow;
SDL_Rect Window::mBox;
SDL_Renderer* Window::mRenderer;
TTF_Font* Window::font;

void Window()
{
}
void Window::Init(std::string title)
{
	//SDL INIT function
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
		throw std::runtime_error("SDL Init failed");
	//preload IMG library, testing flags
	if ((IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) & (IMG_INIT_PNG | IMG_INIT_JPG)) !=
		(IMG_INIT_PNG | IMG_INIT_JPG))
		throw std::runtime_error("IMG Init failed");
	//TTF Initialise
	if (TTF_Init() == -1)
		throw std::runtime_error("TTF Init failed");

	//window size setup
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = 800;
	mBox.h = 600;

	//create window
	mWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		mBox.w, mBox.h, SDL_WINDOW_SHOWN);
	if (mWindow == nullptr)
		throw std::runtime_error("Failed to create window");
	//create renderer
	mRenderer = SDL_CreateRenderer(mWindow, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (mRenderer == nullptr)
		throw std::runtime_error("Failed to create renderer");

	//open font
	font = Window::loadFont("./res/SF_Arch_Rival.ttf", 24);
	if (mRenderer == nullptr)
		throw std::runtime_error("failed to load font");
}

void Window::Quit()
{
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	TTF_CloseFont(font);
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}

void Window::Draw(SDL_Texture *tex, SDL_Rect &destRect, SDL_Rect *clip,	SDL_RendererFlip flip,
				  float angle, int xPivot, int yPivot)
{
	//convert pivots to rectangle's center
	xPivot += destRect.w / 2;
	yPivot += destRect.h / 2;
	//pivot for SDL_RenderCopyEx
	SDL_Point pivot = { xPivot, yPivot } ;
	//draw the texture
	SDL_RenderCopyEx(mRenderer, tex, clip, &destRect, angle, &pivot, flip);
}

void Window::Draw(SDL_Texture *tex, int X_POS, int Y_POS, SDL_Rect *clip, SDL_RendererFlip flip,
				  float angle, int xPivot, int yPivot)
{
	SDL_Rect destRect = {X_POS, Y_POS};
	if (clip != nullptr)
	{
		destRect.w = clip->w; destRect.h = clip->h;
	}
	else
		SDL_QueryTexture(tex, NULL, NULL, &destRect.w, &destRect.h);
	SDL_Point pivot = { xPivot, yPivot } ;
	//draw the texture
	SDL_RenderCopyEx(mRenderer, tex, clip, &destRect, angle, &pivot, flip);
}

SDL_Texture* Window::LoadImage(std::string filename)
{
	SDL_Texture *tex = IMG_LoadTexture(mRenderer, filename.c_str());

	if (tex == nullptr)
		throw std::runtime_error("Failed to load texture");
	return tex;
}

TTF_Font* Window::loadFont(std::string filename, int fontsize)
{
	TTF_Font *fonthandler = TTF_OpenFont(filename.c_str(), fontsize);
	if (fonthandler == nullptr)
		throw std::runtime_error("Failed to load font");
	return fonthandler;
}

SDL_Texture* Window::RenderText(std::string txt, TTF_Font *font,
								SDL_Color color, SDL_Color shaded_color, int quality)
{
	//font_quality { 0:blended, 1:shaded, 2:solid };
	SDL_Surface *surf = nullptr;
	if (quality == 0)
		surf = TTF_RenderText_Solid(font, txt.c_str(), color);
	else if (quality == 2)
		surf = TTF_RenderText_Blended(font, txt.c_str(), color);
	else if (quality == 1)
		surf = TTF_RenderText_Shaded(font, txt.c_str(), color, shaded_color);
	SDL_Texture *tex = SDL_CreateTextureFromSurface(mRenderer, surf);
	SDL_FreeSurface(surf);
	return tex;
}

void Window::Clear()
{
	SDL_RenderClear(mRenderer);
}
void Window::Present()
{
	SDL_RenderPresent(mRenderer);
}
SDL_Rect Window::Box()
{
	//update mBox to match current window size
	SDL_GetWindowSize(mWindow, &mBox.w, &mBox.h);
	return mBox;
}

void Window::ChangeWindowTitle(std::string title)
{
	SDL_SetWindowTitle(mWindow, title.c_str());
}