#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

class CFileHandler
{
public:
	CFileHandler(void);
	~CFileHandler(void);

	int loadMapFromFile(std::string path, std::vector<int> &returnVec);
};

#endif