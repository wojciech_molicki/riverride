#include "Enemy.h"

#include <iostream>


CEnemy::CEnemy(void)
{
	Vx = 0;
	alive = true;
	shooting = false;
	timer.Start();
	type = 0;
	direction = 1;
}

CEnemy::~CEnemy(void)
{
}

SDL_Rect CEnemy::getBox() {
	return box;
}

SDL_Rect CEnemy::getClip() {
	return clip;
}

void CEnemy::setBox(int x, int y, int w, int h)
{
	box.x = x; box.y = y; box.w = w; box.h = h;
}

void CEnemy::setClip(int x, int y, int w, int h)
{
	clip.x = x; clip.y = y; clip.w = w; clip.h = h;
}

bool CEnemy::isMoving()
{
	if (Vx == 0) { return false; }
	else { return true;	}
}

void CEnemy::move() {
	box.x += Vx;
}

void CEnemy::switchShootingOn()
{
	shooting = true;
}

bool CEnemy::isShooting()
{
	return shooting;
}

void CEnemy::switchDirection()
{
	Vx *= -1;
	direction *= -1;
}

void CEnemy::switchMovementOn(int dir)
{
	const int speed = 2;
	Vx = speed * dir;
}

void CEnemy::ressurect()
{
	alive = true;
}

bool CEnemy::isAlive()
{
	return alive;
}

void CEnemy::kill()
{
	alive = false;
}

/* *************************************************************************************************** */

CBoat::CBoat()
{
	setClip(0, 50, 61, 19);
}

void CBoat::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (Vx > 0)
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		else
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
	}
}
void CBoat::shoot(std::list<std::unique_ptr<CShell>> &shells) {}
void CFuel::shoot(std::list<std::unique_ptr<CShell>> &shells) {}
void CBridge::shoot(std::list<std::unique_ptr<CShell>> &shells) {}

CFuel::CFuel()
{
	setClip(50, 0, 30, 50);
}

void CFuel::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (Vx > 0)
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		else
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
	}
}

CBridge::CBridge()
{
	setClip(200, 50, 150, 50);
	closest = false;
	type = BRIDGE;
}

void CBridge::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (Vx > 0)
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		else
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
	}
}

CHeli::CHeli()
{
	setClip(0, 100, 38, 22);
	shooting = true;
}

void CHeli::shoot(std::list<std::unique_ptr<CShell>> &shells)
{
	if (timer.Ticks() > 1000 && shooting == true)
	{
		timer.Restart();
		//CShell *shell = new CShell;
		std::unique_ptr<CShell> shell(new CShell());
		shell->direction = direction;
		shell->box.x = box.x; shell->box.y = box.y + box.h / 2; shell->box.w = 12; shell->box.h = 2;
		shells.emplace_back(std::move(shell));
	}
}

void CHeli::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (Vx > 0)
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		else
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
	}
}

CTank::CTank()
{
	setClip(0, 150, 70, 23);
	direction = 1;
}

void CTank::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (this->direction == 1)
		{
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		}
		if (this->direction == -1)
		{
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
		}
	}
}

void CTank::shoot(std::list<std::unique_ptr<CShell>> &shells)
{
	if (timer.Ticks() > 2000 && shooting == true)
	{
		timer.Restart();
		std::unique_ptr<CShell> shell(new CShell());
		shell->exploding = true;
		shell->direction = direction;
		shell->box.x = box.x; shell->box.y = box.y + box.h / 2; shell->box.w = 4; shell->box.h = 4;
		shells.emplace_back(std::move(shell));
	}
}

CFighter::CFighter()
{
	setClip(150, 100, 44, 16);
	shooting = true;
	type = FIGHTER;
}

void CFighter::shoot(std::list<std::unique_ptr<CShell>> &shells)
{
	if (timer.Ticks() > 1000 && shooting == true)
	{
		timer.Restart();
		std::unique_ptr<CShell> shell(new CShell());
		shell->fast = true;
		shell->direction = Vx;
		shell->box.x = box.x; shell->box.y = box.y + box.h / 2; shell->box.w = 12; shell->box.h = 2;
		shells.emplace_back(std::move(shell));
	}
}

void CFighter::show(SDL_Texture *tex, CPlane &plane)
{
	if (isAlive())
	{
		if (Vx > 0)
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip, SDL_FLIP_HORIZONTAL);
		else
			Window::Draw(tex, box.x - plane.getCamera().x, box.y - plane.getCamera().y, &clip);
	}
}
/* *************************************************************************************************** */