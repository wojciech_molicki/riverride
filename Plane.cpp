#include "Plane.h"

#include <iostream>

CPlane::CPlane()
{
	camera.x = 0; camera.y = 0; camera.w = SCREEN_WIDTH; camera.h = SCREEN_HEIGHT-150;

	setBox(LEVEL_WIDTH / 2, LEVEL_HEIGHT - 100, PLANE_WIDTH, PLANE_HEIGHT);
	starting_pos = box;
	setClip(0, 0, PLANE_WIDTH, PLANE_HEIGHT);
	lives_clip.x = 0; lives_clip.y = 0; lives_clip.w = PLANE_WIDTH; lives_clip.h = PLANE_HEIGHT;
	xVel = 0; yVel = -3;

	lives = 4;
	fuel = 100;

	firing = false;

	timer.Start();
	fuelTimer.Start();
}
void CPlane::show(SDL_Texture* tex) {
	SDL_Rect planePosition = {box.x - camera.x, box.y -camera.y, PLANE_WIDTH, PLANE_HEIGHT  };
	Window::Draw(tex, planePosition, &clip);
}

void CPlane::move() {
	box.x += 5*xVel;//
	//box.x -= camera.w;
	box.y += yVel;//
	//box.y -= camera.h;
	if ((box.x < 0) || (box.x+box.w > LEVEL_WIDTH))	{ box.x -= 5*xVel; }
	//if ((box.y < 0) || (box.y+box.h > LEVEL_HEIGHT))	{ box.y -= 5*yVel; }
	if (box.y <= SCREEN_HEIGHT / 2 - 1) box.y = LEVEL_HEIGHT - (SCREEN_HEIGHT / 2);
	//std::cout << "box.x=" << box.x << " box.y=" << box.y << " box.w=" << box.w << " box.h=" << box.h << std::endl;

	fuelDrain();
	fire();

	//std::cout << "fuel:" << fuel << std::endl;
}

void CPlane::setCamera()
{
	camera.x = ( box.x + box.w / 2 ) - SCREEN_WIDTH / 2;
	camera.y = ( box.y + box.h / 2 ) - SCREEN_HEIGHT / 2;

	if (camera.x < 0) camera.x = 0;
	if (camera.y < 0) camera.y = 0;
	if (camera.x > LEVEL_WIDTH - camera.w) camera.x = LEVEL_WIDTH - camera.w;
	if (camera.y > LEVEL_HEIGHT - camera.h) camera.y = LEVEL_HEIGHT - camera.h;

	//std::cout << "yPlane=" << plane.getBox().y << std::endl;
}

void CPlane::fire() {
	//std::cout << shells.size() << std::endl;
	if ((timer.Ticks() > PLANE_SHELL_FREQ) && firing)
	{
		timer.Restart();

		std::unique_ptr<SDL_Rect> shell_ptr(new SDL_Rect);
		
		shell_ptr->x = box.x + box.w / 2; shell_ptr->y = box.y;
		shell_ptr->w = 2; shell_ptr->h = 10;
		shells.push_back(std::move(shell_ptr));
	}
}

void CPlane::setFire(bool value) {
	firing = value;
}

void CPlane::setVx(int value) {
	xVel = value;
}
void CPlane::setVy(int value) {
	yVel = value;
}

void CPlane::setStartingPos(int x, int y)
{
	starting_pos.x = x; starting_pos.y = y;
}

SDL_Rect CPlane::getBox() {
	return box;
}

SDL_Rect CPlane::getClip() {
	return clip;
}

SDL_Rect CPlane::getLivesClip() {
	return lives_clip;
}

SDL_Rect CPlane::getCamera() {
	return camera;
}

void CPlane::setBox(int x, int y, int w, int h)
{
	box.x = x; box.y = y; box.w = w; box.h = h;
}

void CPlane::setClip(int x, int y, int w, int h)
{
	clip.x = x; clip.y = y; clip.w = w; clip.h = h;
}

std::list<std::unique_ptr<SDL_Rect>> *CPlane::getShellsFired()
{
	return &shells;
}

void CPlane::moveShots()
{
	for (std::list<std::unique_ptr<SDL_Rect>>::iterator it = shells.begin(); it != shells.end(); it++)
	{
		(*it)->y -= PLANE_SHELL_SPEED;
		if ((*it)->y < camera.y)
		{
			it->release();
			shells.erase(it++);
		}
	}
}

int CPlane::getLives()
{
	return lives;
}

void CPlane::removeLife()
{
	fuelTimer.Restart();
	fuel = 100;

	lives--;
	setBox(starting_pos.x, starting_pos.y, PLANE_WIDTH, PLANE_HEIGHT);
}

void CPlane::fuelDrain()
{
	if (fuelTimer.Ticks() > 200)
	{
		fuelTimer.Restart();
		fuel--;
	}
	if (fuel < 0)
	{
		fuelTimer.Restart();
	}
}

void CPlane::refuel()
{
	fuel += 30;
	if (fuel > 100) fuel = 100;
}

void CPlane::resetVxLeft()
{
	if (xVel < 0)
		xVel = 0;
}

void CPlane::resetVxRight()
{
	if (xVel > 0)
		xVel = 0;
}

void CPlane::resetVyDown()
{
	if (yVel == PLANE_MIN_SPEED)
		yVel = PLANE_REGULAR_SPEED;
}

void CPlane::resetVyUp()
{
	if (yVel == PLANE_MAX_SPEED)
		yVel = PLANE_REGULAR_SPEED;
}

int CPlane::getFuel()
{
	return fuel;
}

void CPlane::reset()
{
	camera.x = 0; camera.y = 0; camera.w = SCREEN_WIDTH; camera.h = SCREEN_HEIGHT-150;

	setBox(LEVEL_WIDTH / 2, LEVEL_HEIGHT - 100, PLANE_WIDTH, PLANE_HEIGHT);
	starting_pos = box;
	setClip(0, 0, PLANE_WIDTH, PLANE_HEIGHT);
	lives_clip.x = 0; lives_clip.y = 0; lives_clip.w = PLANE_WIDTH; lives_clip.h = PLANE_HEIGHT;
	xVel = 0; yVel = -3;

	lives = 4;
	fuel = 100;

	firing = false;

	timer.Start();
	fuelTimer.Start();

	for (std::list<std::unique_ptr<SDL_Rect>>::iterator it = shells.begin(); it != shells.end(); it++)
	{
		it->release();
	}
	shells.clear();
}