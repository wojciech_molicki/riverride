#include "Shell.h"

CShell::CShell(void)
{
	exploding = false;
	exploded = false;
	fast = false;
	distance = 200;
	radius = 15;
}

CShell::~CShell(void)
{
}

bool CShell::doIExplode()
{
	if (exploding && distance <= 0)
	{
		return true;
	}
	return false;
}