#ifndef CGAME_H
#define CGAME_H

#include "SDL.h"
#include "Window.h"

#include "Timer.h"
#include "FileHandler.h"
#include "Plane.h"
#include "Enemy.h"
#include "Tile.h"
#include "Globals.h"
#include "GameHelpers.h"

#include <memory>
#include <list>

class CGame
{
public:
	CGame(void);
	~CGame(void);

	void init();
	void render();
	void update();
	void cleanup();

	bool running() { return m_bRunning; }
	
private:
	CTile board[TILE_NUMBER];
	Timer timer;
	CPlane plane;
	CFighter menu_fighter;
	CHeli menu_heli;
	
	bool m_bRunning;
	int score;
	int frameLimiter;

	enum gamestates {START, GAME, GAMEOVER, PAUSE};
	gamestates gamestate;

	SDL_Color fontcolor;

	SDL_Texture *clip_image;
	SDL_Texture *score_tex;
	SDL_Texture *gameover_tex1;
	SDL_Texture *gameover_tex2;

	std::vector<int> mapVector;
	std::list<std::unique_ptr<CEnemy>> enemyList;
	std::list<std::unique_ptr<CEnemy>> powerupList;
	std::list<std::unique_ptr<CShell>> enemyShells;

	void drawLives();
	void drawFuelGauge();
	void drawScore();
	void drawMenus();
	void drawGameOverScreen();

	void loadInitial();
	void setTileTypes();
	void setTilePlaces();
	void resetLevel();

	void drawTiles();
	void drawEnemies();
	void drawPowerups();
	void drawBottomBar();
	void drawEnemyShells();

	void handleShots();
	void handleEnemyShots();

	bool groundCollision(SDL_Rect A);
	bool enemyCollision(const SDL_Rect A);
	void collideShellsWithEnemies();
	void collideShellsWithPowerups();
	bool collidePlaneWithEnemyShells();
	void powerupsCollision();

	bool standingOnCollapsingBridge(SDL_Rect A);
	bool bridgeDead();

	void updateScoreTexture(int amount);
	void enemyMovement();

	void handleEvents();
	void handleMenuEvents();

	void gameOverHandler();
};

#endif
