#ifndef PLANE_H
#define PLANE_H

#include "SDL.h"
#include "Tile.h"
#include "Globals.h"
#include <list>
#include "Timer.h"
#include "Window.h"
#include <memory>

class CPlane {
protected:
	//dimensions

	SDL_Rect clip;
	SDL_Rect lives_clip;
	SDL_Rect box;
	SDL_Rect camera;
	SDL_Rect starting_pos;

	Timer timer;
	Timer fuelTimer;

	std::list<std::unique_ptr<SDL_Rect>> shells;

	int lives;
	int fuel;

	int xVel, yVel;

	void hitAnimation();
	void fuelDrain();

public:

	CPlane();
	//chaange position and vel depending on input
	void handleInput();

	//moves plane on tileboard
	void move();
	void show(SDL_Texture *tex);
	void fire();

	void removeLife();
	void refuel();
	void moveShots();

	void setVx(int value);
	void setVy(int value);

	void resetVxRight();
	void resetVxLeft();

	void setBox(int x, int y, int w, int h);
	void setClip(int x, int y, int w, int h);
	void setStartingPos(int x, int y);

	void resetVyUp();
	void resetVyDown();

	void setCamera();

	bool swapped;
	bool firing;

	
	std::list<std::unique_ptr<SDL_Rect>> *getShellsFired();

	SDL_Rect getClip();
	SDL_Rect getLivesClip();
	SDL_Rect getBox();
	SDL_Rect getCamera();
	int getLives();
	int getFuel();

	void reset();

	void setFire(bool value);
};

#endif
