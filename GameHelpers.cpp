#include "GameHelpers.h"

bool checkCollision(const SDL_Rect A, const SDL_Rect B)
{
	//sides
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//determine sides of sdl_rect A	and B
	leftA = A.x; rightA = A.x + A.w; topA = A.y; bottomA = A.y + A.h;
	leftB = B.x; rightB = B.x + B.w; topB = B.y; bottomB = B.y + B.h;

	if (bottomA <= topB) return false;
	if (topA >= bottomB) return false;
	if (leftA >= rightB) return false;
	if (rightA <= leftB) return false;

	return true;
}

bool circleCollision(const SDL_Rect A, const Circle R)
{
	return true;
}

double pointToPointDistance(int Ax, int Ay, int Bx, int By)
{
	return SDL_sqrt(SDL_pow(Bx - Ax, 2.0) + SDL_pow(By - Ay, 2.0));
}