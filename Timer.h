#ifndef TIMER_H
#define TIMER_H
class Timer
{
public:
	Timer();
	void Start();
	void Stop();
	void Pause();
	void Unpause();
	//returns elapsed ticks, restarts timer
	int Restart();

	//get elapsed ticks
	int Ticks() const;
	//check if started
	bool Started() const;
	//check if paused
	bool Paused() const;
private:
	int mStartTicks, mPausedTicks;
	bool mStarted, mPaused;
};

#endif