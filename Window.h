#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

class Window
{
public:
	//starts SDL, IMG, TTF, creates window and renderer
	static void Init(std::string title = "Window");
	//quits SDL, IMG and TTF, destroys objects
	static void Quit();

	//textures
	static SDL_Texture* LoadImage(std::string filename);

	static void Draw(SDL_Texture *tex, SDL_Rect &destRect, SDL_Rect *clip = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE,
		float angle = 0.0, int xPivot = 0, int yPivot = 0);
	static void Draw(SDL_Texture *tex, int X_POS, int Y_POS, SDL_Rect *clip = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE,
		float angle = 0.0, int xPivot = 0, int yPivot = 0);
	//text
	static TTF_Font* loadFont(std::string filename, int fontsize);
	static SDL_Texture* RenderText(std::string txt, TTF_Font *font,
		SDL_Color color,  SDL_Color shaded_color, int quality = 0);
	//clear window
	static void Clear();
	//present renderer (update screen)
	static void Present();
	//get window box
	static SDL_Rect Box();
	static void Window::ChangeWindowTitle(std::string title);

	static SDL_Window* mWindow;
	static SDL_Rect mBox;
	static SDL_Renderer* mRenderer;

	static TTF_Font* font;

private:
};

#endif
