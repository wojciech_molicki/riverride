#ifndef SHELL_H
#define SHELL_H

#include "Timer.h"

class CShell
{
public:
	CShell(void);
	~CShell(void);

	int direction;
	int velocity;
	bool exploding;
	bool exploded;
	bool fast;
	int distance;
	int radius;

	bool doIExplode();

	struct Rect { int x; int y; int w; int h;} box;

	Timer timer;
};

#endif
