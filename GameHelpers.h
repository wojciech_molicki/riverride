#ifndef GAMEHELPER_H
#define GAMEHELPER_H

#include "SDL.h"

struct Circle {
	int x; int y;
	int r;
};

bool checkCollision(const SDL_Rect A, const SDL_Rect B);
bool circleCollision(const SDL_Rect A, const Circle R);

double pointToPointDistance(int Ax, int Ay, int Bx, int By);

#endif