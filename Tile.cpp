#include "Tile.h"

CTile::CTile()
{
	box.w = TILE_WIDTH; box.h = TILE_HEIGHT;
}

void CTile::set_pos(int x, int y)
{
	box.x = x;
	box.y = y;
}

void CTile::set_type(int tileType)
{
	type = tileType;
	clip.x = 100; clip.y = type*TILE_HEIGHT; clip.w = TILE_WIDTH; clip.h = TILE_HEIGHT;
}

SDL_Rect CTile::get_box()
{
	return box;
}

SDL_Rect CTile::get_clip()
{
	return clip;
}

void CTile::show()
{
}

int CTile::get_type()
{
	return type;
}