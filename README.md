riverride
=========

C++/SDL2.0 project, implementing riverride game from Commodore64.

screenshots: [https://github.com/wmolicki/riverride/wiki](https://github.com/wmolicki/riverride/wiki)
