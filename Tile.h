#ifndef TILE_H
#define TILE_H

#include "SDL.h"
#include "Globals.h"

class CTile {
private:
	//dimensions of file
	SDL_Rect box;
	SDL_Rect clip;
	//type of tile
	int type;

public :
	CTile();
	void show();
	int get_type();
	void set_type(int tileType);
	void set_pos(int x, int y);
	SDL_Rect get_box();
	SDL_Rect get_clip();
};

#endif